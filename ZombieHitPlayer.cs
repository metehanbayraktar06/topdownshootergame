using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieHitPlayer : MonoBehaviour
{
    CharControl charControl;
    private void Start()
    {
        charControl = GameObject.FindGameObjectWithTag("Player").GetComponent<CharControl>();
    }
    private void OnTriggerEnter(Collider other)
    {
        int Damage;
         if (other.gameObject.tag == "ZombieHit")
        {
            Damage = 20;
            charControl.hitPlayer(Damage);
        }
         if (other.gameObject.tag == "ZombieBossHit")
        {
            Damage = 70;
            charControl.hitPlayer(Damage);
        }
    }
}
