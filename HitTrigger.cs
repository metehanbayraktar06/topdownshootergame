using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTrigger : MonoBehaviour
{
    public CharControl charScript;
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "enemy")
        {          
            charScript.hitTrigger = true;
        }
        if (other.tag == "dieCol")
        {
            charScript.hitTrigger = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "enemy")
        {
            charScript.hitTrigger = false;
        }
    }
}
