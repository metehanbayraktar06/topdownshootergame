using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public Transform GunHoldPos;
    public GameObject PlayerRightHand;
    Rigidbody Rb;
    public CharControl playerScript;
    float speed = 10f;
    public bool enemyhand;
    public EnemyScript enemyScript;
    float timer = 0f;
    public float rotateSpeed;
    bool noAmmo = false;
    public GameObject BulletNumberUi;
    public AudioSource takeGunAudio;
    private void Start()
    {
        Rb = GetComponent<Rigidbody>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "takegun" )
        {
          //  playerScript.BulletNumber = transform.GetChild(0).childCount;
        }

    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "takegun" && !enemyhand && timer < 0 && !playerScript.hasGun && !playerScript.hasShotgun && !noAmmo && !playerScript.hasRifle)
        {
            CancelInvoke("DestroyMe");
            takeGunAudio.Play();
            transform.position = GunHoldPos.position;
            transform.eulerAngles = GunHoldPos.eulerAngles;
            transform.SetParent(PlayerRightHand.transform); 
            transform.SetSiblingIndex(0);
            Rb.isKinematic = true;
            playerScript.hasGun = true;
           
            for (int i = 0; i < BulletNumberUi.transform.childCount; i++)
            {
                BulletNumberUi.transform.GetChild(i).gameObject.SetActive(true);
            }
            BulletNumberUi.SetActive(true);
            playerScript.fireNum = 0;
        }
        if (other.tag == "enemy")
        {
            if (enemyScript != null)
            {
                if (!enemyScript.died)
                {
                    enemyhand = true;
                }
            }
            
            
        }
    }
    void DestroyMe()
    {
        Destroy(gameObject);
    }
    private void Update()
    {
        if (transform.GetChild(0).childCount != 0 && Rb.isKinematic == false)
        {
           
            transform.Rotate(0, 0, 30 * rotateSpeed);
            Invoke("DestroyMe", 20);
        }
        else if (transform.GetChild(0).childCount == 0 && Rb.isKinematic == true)
        {
            noAmmo = true;
            Destroy(gameObject, 10);
        }
        timer -= Time.deltaTime;
        if (Input.GetMouseButtonDown(1))
        {
            timer = 0.5f;
        }  
    }
}
