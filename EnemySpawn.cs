using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public int spawnPistolTime;
    public int spawnShotgunTime;
    public int spawnZombieTime;
    public int spawnZombieBossTime;

    public GameObject[] EnemyGo;
    public Transform[] spawnPlaces = new Transform[4];
    void Start()
    {
        InvokeRepeating("enemySpawnPistol", spawnPistolTime, spawnPistolTime);
        InvokeRepeating("enemySpawnShotgun", spawnShotgunTime, spawnShotgunTime);
        InvokeRepeating("enemySpawnZombie", spawnZombieTime, spawnZombieTime);
        InvokeRepeating("enemySpawnZombieBoss", spawnZombieBossTime, spawnZombieBossTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void enemySpawnPistol()
    {
        Instantiate(EnemyGo[0], spawnPlaces[Random.Range(0,spawnPlaces.Length)].position, Quaternion.identity);
    }
    void enemySpawnShotgun()
    {
        Instantiate(EnemyGo[1], spawnPlaces[Random.Range(0, spawnPlaces.Length)].position, Quaternion.identity);
    }
    void enemySpawnZombie()
    {
        Instantiate(EnemyGo[2], spawnPlaces[Random.Range(0, spawnPlaces.Length)].position, Quaternion.identity);
    }
    void enemySpawnZombieBoss()
    {
        Instantiate(EnemyGo[3], spawnPlaces[Random.Range(0, spawnPlaces.Length)].position, Quaternion.identity);
    }
}
