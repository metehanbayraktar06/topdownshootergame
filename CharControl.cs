using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CharControl : MonoBehaviour
{
    public MusicPitch musicPitch;
    bool timeForward;
    public Animator PlayerAnim;
    public CharacterController Controller;
    public float speed = 6f;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;
    bool isMoving;
    Vector3 direction;
     Transform NearEnemyPos;

    public bool hasHitten;
    public CharControl charcont;

    public Transform GunHoldPos;
    public Transform PlayerRightHand;
    public bool hasGun;
    public bool hasShotgun;
    public GameObject Projectile;
    public GameObject ShotgunBullets;

    bool gunFired;
    public Transform BulletExitPlace;
    float shootAngle;
    float turnspeed;
    public Camera cam;
    Vector3 target;
    Plane plane;
    Ray ray;
    float distance;
    RaycastHit hit;
    public GameObject TargetCube;
    private Vector3 closestEnemy;
    public bool gunThrowed = false;
    float time;
    float x;
    float y;
    bool action;
    float RateFire;
    public bool hitTrigger = false;
    public int BulletNumber = 3;
    public bool dead;
    public GameObject Lazer;
    public GameObject ShotgunLazer;
    public GameObject BulletNumUi;
    public GameObject RifleNumUi;
    public int fireNum;
    public MuzzleFlash muzzleFlash;
    public AudioSource AudioPistolShoot;
    public AudioSource AudioShotgunShoot;
    public AudioSource AudioRifleShoot;
    public AudioSource AudioDeath;
    public Collider CapsCollider;
    public int MaxHealth = 100;
    public int currentHealth;
    public HealthBar healthBar;
    public AudioSource hitSound;
    public AudioSource[] allSoundsToLerp;
    [Range(0, 1)]
    public float lerpSpeed;
    public bool hasRifle;
    void Start()
    {
        currentHealth = MaxHealth;
        healthBar.SetMaxHealth(MaxHealth);
        dead = false;
        hasGun = false;
        hasShotgun = false;
        //  Cursor.visible = false;
        for (int i = 0; i < RifleNumUi.transform.childCount; i++)
        {
            RifleNumUi.transform.GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < BulletNumUi.transform.childCount; i++)
        {
            BulletNumUi.transform.GetChild(i).gameObject.SetActive(false);
        }
    }
  
    void turnToMousePos()
    {
        if (Input.GetMouseButton(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            plane = new Plane(Vector3.up, Vector3.zero);
            if (plane.Raycast(ray, out distance))
            {
                target = ray.GetPoint(distance);
                Vector3 direction = target - transform.position;
                float rotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(0, rotation, 0);
            }
        }
    }
    public IEnumerator punch()
    {
        if ( Input.GetMouseButtonUp(0) && hitTrigger && !hasGun  && !hasShotgun && !hasRifle)
        {
          //  StopCoroutine(ActionE(.03f));
          //  StartCoroutine(ActionE(.03f));
            hasHitten = true;
            PlayerAnim.SetBool("run", false);
            PlayerAnim.SetBool("punch", true);
            PlayerAnim.SetBool("shootidle", false);

            transform.LookAt(NearEnemyPos);
            yield return new WaitForSeconds(0.3f);
            PlayerAnim.SetBool("punch", false);
            PlayerAnim.SetBool("stop", false);
            




        }

    }
    void DuvarinArkasindanAtesEtmemek()
    {
        float range = 10f;
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, range))
        {

            //Debug.DrawRay(transform.position, transform.forward, Color.black);
        }



    }
   
    void Update()
    {
        if (currentHealth <= 0)
        {
            diePlayer();
        }
        //DuvarinArkasindanAtesEtmemek();
        StartCoroutine(punch());

        
        RateFire -= Time.deltaTime;
       // StartCoroutine(punch());

        turnToMousePos();
        GunThrow();
        ShotGunThrow();
        RifleThrow();
         x = Input.GetAxisRaw("Horizontal");
         y = Input.GetAxisRaw("Vertical");
        /*
        float time = (x != 0 || y != 0) ? 1f : .3f;
        float lerpTime = (x != 0 || y != 0) ? .05f : .5f;

        time = action ? 1 : time;
        lerpTime = action ? .1f : lerpTime;

        Time.timeScale = Mathf.Lerp(Time.timeScale, time, lerpTime);
        */

        // StartCoroutine(CharPunch());
        if (Input.GetMouseButton(0))
        {
            if (hasShotgun || hasRifle)
            {
                PlayerAnim.SetBool("ShotgunIdle", true);
                PlayerAnim.SetBool("ShotgunRun", false);

            }
            if (hasGun)
            {
                PlayerAnim.SetBool("shootidle",true);
                PlayerAnim.SetBool("pistolwalk", false);
            }
            if (!hasGun &&!hasShotgun && !hasRifle)
            {
                PlayerAnim.SetBool("run", false);
                PlayerAnim.SetBool("stop", true);
            }
        }
        if ( !Input.GetMouseButton(0) && !gunFired)
        {
            CharMovement();
        }
        if (hasGun && !gunFired )
        {
           StartCoroutine(Shoot());

        }
        if (hasShotgun)
        {
            StartCoroutine(ShootShotgun());
        }
        if (hasRifle)
        {
            StartCoroutine(ShootRifle());
            
        }
       

    }
    void LerpSound(AudioSource[] SoundsToLerp , float MaxOrMinValue , float LerpSpeed)
    {
        for (int i = 0; i < SoundsToLerp.Length; i++)
        {
            SoundsToLerp[i].pitch = Mathf.Lerp(SoundsToLerp[i].pitch, MaxOrMinValue, LerpSpeed);
        }
        
    }
    // karaktar hareket etme ve animasyonları
    void CharMovement()
    {
        
        

        direction = new Vector3(x, 0, y).normalized;
        // Eğer Hareket olursa
        if (direction.magnitude >= 0.1f)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 1.2f, lerpSpeed);
            LerpSound(allSoundsToLerp, 1.2f, lerpSpeed);

           // allSoundsToLerp[0].pitch = Mathf.Lerp(allSoundsToLerp[0].pitch, 1.2f, lerpSpeed);
            //  musicPitch.pitchValue = Mathf.Lerp(musicPitch.pitchValue, 1.2f, lerpSpeed);

            if (!hasGun && !hasShotgun && !hasRifle)
            {                
                PlayerAnim.SetBool("stop", false);
                PlayerAnim.SetBool("run", true);
                

                PlayerAnim.SetBool("pistolwalk", false);
                Lazer.SetActive(false);
                ShotgunLazer.SetActive(false);
                speed = 6f;
            }
            if (hasGun )
            {
                Lazer.SetActive(true);
                PlayerAnim.SetBool("run", false);

                PlayerAnim.SetBool("shootidle", false);
                PlayerAnim.SetBool("pistolwalk", true);
                speed = 4f;
            }
            if (hasShotgun || hasRifle)
            {
                ShotgunLazer.SetActive(true);

                PlayerAnim.SetBool("run", false);

                PlayerAnim.SetBool("ShotgunIdle", false);
                PlayerAnim.SetBool("ShotgunRun", true);
                speed = 4f;

            }


            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            
                Controller.Move(direction * speed * Time.deltaTime);          
        }
        // Eğer Hareket olmazsa
        if (direction.magnitude <= 0.1f)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 0.1f, lerpSpeed);
            LerpSound(allSoundsToLerp, 0.7f, lerpSpeed);
            //  musicPitch.pitchValue = Mathf.Lerp(musicPitch.pitchValue, 0.7f , lerpSpeed);

            if (gunThrowed)
            {
               // Time.timeScale = 1;
            }

            //   Time.timeScale = 0.3f;

            if (hasShotgun || hasRifle)
            {
                PlayerAnim.SetBool("ShotgunIdle", true);
                PlayerAnim.SetBool("ShotgunRun", false);
                PlayerAnim.SetBool("run", false);
                PlayerAnim.SetBool("stop", false);
            }
            
              
            if (hasGun)
            {
                PlayerAnim.SetBool("shootidle", true);
                PlayerAnim.SetBool("pistolwalk", false);
                PlayerAnim.SetBool("run", false);
                PlayerAnim.SetBool("stop", false);


            }
            if (!hasGun && !hasShotgun && !hasRifle)
            {
                PlayerAnim.SetBool("shootidle", false);

                PlayerAnim.SetBool("stop", true);
                PlayerAnim.SetBool("run", false);
            }
           
        }
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "enemy")
        {
            NearEnemyPos = other.transform;
            //StartCoroutine(CharPunch());
            
        }
      
       
    }
    public void TakeHealth(int Health)
    {
        currentHealth += Health;
        if (currentHealth > 100)
        {
            currentHealth = 100;
        }
        healthBar.SetHealth(currentHealth);
    }
    public void hitPlayer(int damage)
    {
        if (currentHealth > 0)
        {
            hitSound.Play();
        }

        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);


    }

    private void OnCollisionEnter(Collision col)
    {
        int Damage;
        if (col.gameObject.tag == "bulletplayer"  )
        {
            Damage = 30;
            hitPlayer(Damage);
            Destroy(col.gameObject);
           
        }
    }
    public void diePlayer()
    {
        CapsCollider.enabled = false;
        AudioDeath.Play();
        

        PlayerAnim.SetBool("die", true);


        dead = true;
        charcont.enabled = false;
    }
    IEnumerator Shoot()
    {
        if (Input.GetMouseButtonUp(0) && hasGun && RateFire < 0 && PlayerRightHand.GetChild(0).GetChild(0).childCount != 0)
        {
            RateFire = 1f;
          
            gunFired = true;
            AudioPistolShoot.Play();

            PlayerRightHand.GetChild(0).GetChild(0).GetChild(0).parent = null;
            BulletNumUi.transform.GetChild(fireNum).gameObject.SetActive(false);
            fireNum += 1;
            StartCoroutine(muzzleFlash.ActivatePistol());


            PlayerAnim.SetBool("shoot", true);


            float distance = 15;

            Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            position = cam.ScreenToWorldPoint(position);
            direction = (transform.position - position).normalized;
            GameObject Bullet = Instantiate(Projectile, BulletExitPlace.position, Quaternion.identity);
            position = new Vector3(0,0,0);
            Bullet.transform.eulerAngles = transform.eulerAngles;
            // Bullet.GetComponent<Rigidbody>().AddForce(Bullet.transform.forward * 1000f, ForceMode.Impulse);
            //  Bullet.transform.Translate(direction * 10 * Time.deltaTime);
            //  Bullet.transform.LookAt(position);
           // Bullet.transform.LookAt(TargetCube.transform);
         //   Bullet.transform.forward = transform.forward;
         //   Bullet.transform.eulerAngles = BulletExitPlace.eulerAngles;
            // Bullet.GetComponent<Rigidbody>().AddForce(Bullet.transform.forward * 1000f, ForceMode.Impulse);

            yield return new WaitForSeconds(0.2f);
            PlayerAnim.SetBool("shoot", false);
            PlayerAnim.SetBool("shootidle", false);

            gunFired = false;
            
        }
        
    }
    IEnumerator ShootShotgun()
    {
        if (Input.GetMouseButtonUp(0)  && RateFire < 0 && PlayerRightHand.GetChild(0).GetChild(0).childCount != 0)
        {
            RateFire = 1f;
            StopCoroutine(ActionE(.03f));
            StartCoroutine(ActionE(.03f));
            gunFired = true;
            AudioShotgunShoot.Play();

            PlayerRightHand.GetChild(0).GetChild(0).GetChild(0).parent = null;
            BulletNumUi.transform.GetChild(fireNum).gameObject.SetActive(false);
            fireNum += 1;
            StartCoroutine(muzzleFlash.ActivateShotgun());




            PlayerAnim.SetBool("ShotgunShoot", true);


            float distance = 15;

            Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            position = cam.ScreenToWorldPoint(position);
            direction = (transform.position - position).normalized;
            GameObject Bullet = Instantiate(ShotgunBullets, BulletExitPlace.position, Quaternion.identity);
            position = new Vector3(0, 0, 0);
            
            Bullet.transform.LookAt(TargetCube.transform);
        
            yield return new WaitForSeconds(0.2f);
            PlayerAnim.SetBool("ShotgunShoot", false);
            PlayerAnim.SetBool("ShotgunIdle", false);

            gunFired = false;

        }

    }
    IEnumerator ShootRifle()
    {
        if (Input.GetMouseButton(0) && RateFire < 0 && PlayerRightHand.GetChild(0).GetChild(0).childCount != 0)
        {
            RateFire = 0.2f;
            StopCoroutine(ActionE(.03f));
            StartCoroutine(ActionE(.03f));
            gunFired = true;
            AudioRifleShoot.Play();

            PlayerRightHand.GetChild(0).GetChild(0).GetChild(0).parent = null;
            RifleNumUi.transform.GetChild(fireNum).gameObject.SetActive(false);
            fireNum += 1;
            StartCoroutine(muzzleFlash.ActivateShotgun());




            PlayerAnim.SetBool("ShotgunShoot", true);


            float distance = 15;

            Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            position = cam.ScreenToWorldPoint(position);
            direction = (transform.position - position).normalized;
            GameObject Bullet = Instantiate(Projectile, BulletExitPlace.position, Quaternion.identity);
            position = new Vector3(0, 0, 0);

            Bullet.transform.LookAt(TargetCube.transform);

            yield return new WaitForSeconds(0.2f);
            PlayerAnim.SetBool("ShotgunShoot", false);
            PlayerAnim.SetBool("ShotgunIdle", false);

            gunFired = false;

        }

    }
    void GunThrow()
    {
        if (hasGun && Input.GetMouseButtonDown(1) )
        {
           
          
            PlayerAnim.SetBool("shootidle", false);
            PlayerAnim.SetBool("ShotgunIdle", false);
            PlayerAnim.SetBool("pistolwalk", false);
            PlayerAnim.SetBool("ShotgunRun", false);

            PlayerAnim.SetBool("stop", true);

            Rigidbody gunRigidBody = PlayerRightHand.GetChild(0).GetComponent<Rigidbody>();
            if (gunRigidBody != null)
            {
                gunRigidBody.gameObject.transform.eulerAngles = new Vector3(90, 0, 0);
                gunRigidBody.gameObject.transform.eulerAngles = new Vector3(90, 0, 0);

                gunRigidBody.transform.SetParent(null);
                gunRigidBody.isKinematic = false;
            }
            
             gunThrowed = true;
            //  gunRigidBody.transform.LookAt(closestEnemy);

            for (int i = 0; i < BulletNumUi.transform.childCount; i++)
            {
                BulletNumUi.transform.GetChild(i).gameObject.SetActive(false);
            }
            hasGun = false;
            hasShotgun = false;

        }
    }
    void ShotGunThrow()
    {
        if (hasShotgun && Input.GetMouseButtonDown(1) )
        {


            PlayerAnim.SetBool("shootidle", false);
            PlayerAnim.SetBool("ShotgunIdle", false);
            PlayerAnim.SetBool("pistolwalk", false);
            PlayerAnim.SetBool("ShotgunRun", false);

            PlayerAnim.SetBool("stop", true);
            
            Rigidbody gunRigidBody = PlayerRightHand.GetChild(0).GetComponent<Rigidbody>();
            if (gunRigidBody != null)
            {
                gunRigidBody.gameObject.transform.eulerAngles = new Vector3(90, 0, 0);

                gunRigidBody.transform.SetParent(null);
                gunRigidBody.isKinematic = false;
                gunThrowed = true;
            }

            //  gunRigidBody.transform.LookAt(closestEnemy);

            for (int i = 0; i < BulletNumUi.transform.childCount; i++)
            {
                BulletNumUi.transform.GetChild(i).gameObject.SetActive(false);
            }
            hasGun = false;
            hasShotgun = false;

        }
    }
    void RifleThrow()
    {
        if (hasRifle && Input.GetMouseButtonDown(1))
        {


            PlayerAnim.SetBool("shootidle", false);
            PlayerAnim.SetBool("ShotgunIdle", false);
            PlayerAnim.SetBool("pistolwalk", false);
            PlayerAnim.SetBool("ShotgunRun", false);

            PlayerAnim.SetBool("stop", true);

            Rigidbody gunRigidBody = PlayerRightHand.GetChild(0).GetComponent<Rigidbody>();
            if (gunRigidBody != null)
            {
                gunRigidBody.gameObject.transform.eulerAngles = new Vector3(90, 0, 0);

                gunRigidBody.transform.SetParent(null);
                gunRigidBody.isKinematic = false;
                gunThrowed = true;
            }

            //  gunRigidBody.transform.LookAt(closestEnemy);

            for (int i = 0; i < RifleNumUi.transform.childCount; i++)
            {
                RifleNumUi.transform.GetChild(i).gameObject.SetActive(false);
            }
            hasGun = false;
            hasShotgun = false;
            hasRifle = false;

        }
    }
    IEnumerator ActionE(float time)
    {
        action = true;
        yield return new WaitForSecondsRealtime(time);
        action = false;
    }
}
