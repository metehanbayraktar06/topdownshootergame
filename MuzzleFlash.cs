using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour
{
    public GameObject FlashHolderShotgun;
    public GameObject FlashHolderPistol;

    
 
    public IEnumerator ActivateShotgun()
    {
        if (FlashHolderShotgun != null)
        {
            for (int i = 0; i < 4; i++)
            {
                FlashHolderShotgun.transform.GetChild(i).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.03f);
                FlashHolderShotgun.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        


    }
    public IEnumerator ActivatePistol()
    {
        if (FlashHolderPistol != null)
        {
            for (int i = 0; i < 4; i++)
            {
                FlashHolderPistol.transform.GetChild(i).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.03f);
                FlashHolderPistol.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

    }

}
