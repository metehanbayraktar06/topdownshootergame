using UnityEngine;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour
{
    public CharControl charControl;
    public float speed;
    public float stoppingDistance;
    public float retreatDistance;

    private float timeBtwShots;
    public float startTimeBtwShots;

    public GameObject Projectile;
    public Transform Player;

    private Vector3 FakePlayerTransform;
    private Vector3 FakeMyTransform;

    public Animator EnemyAnim;
    // public Transform ProjectileExitPlace;

    bool movimentBool;

    float timer = 1;

    public NavMeshAgent agent;

    public bool hasGun;

    public Transform ProjectileExitPlace;

    public EnemyScript enemyScript;

    public bool died;

    Rigidbody enemyRB;
    public Collider enemyCol;
    public Collider enemyBoxCol;
    public GameObject gun;
    public Gun gunScript;
    public Rigidbody gunRb;
    public GameObject DieCol;
    Vector3 lookat;
    public GameObject Blood;
    bool eXecuteOnce;
    public MuzzleFlash muzzleFlash;
    public AudioSource DeathSound;
    public AudioSource FireSound;
    bool canAttack = true;
    public LayerMask playerLayerMask;
    float timeBtw ;
    // public GameObject Gun;
    void Start()
    {
        

        died = false;
        enemyRB = GetComponent<Rigidbody>();
        hasGun = true;
        // Player = GameObject.FindGameObjectWithTag("Player").transform;

    }


    void Update()
    {
        timeBtw -= Time.deltaTime;
        if (!canAttack && timeBtw <= 0)
        {
            stoppingDistance--;
            timeBtw = 0.1f;
        }
        DuvarinArkasindanAtesEtmemek();
        lookat = new Vector3(Player.position.x, transform.position.y, Player.position.z);

        if (died)
        {

            transform.position = this.transform.position;
        }
        if (!died)
        {
            transform.LookAt(lookat);

        }
        // enemy playera sabit bakıyor ve onun son pozisyonlarını alıyor.
        FakePlayerTransform = new Vector3(Player.position.x, 0, Player.position.z);
        FakeMyTransform = new Vector3(transform.position.x, 0, transform.position.z);

        timer -= Time.deltaTime;
        // StartCoroutine(EnemyMovementAndAnims());



        if (died == false)
        {
            EnemyMovementAndAnims();
        }
    }
        

    void EnemyMovementAndAnims()
    {


        if (Vector3.Distance(transform.position, Player.position) > stoppingDistance && timer < 0 )
        {

            EnemyAnim.SetBool("run", true);
            EnemyAnim.SetBool("stop", false);


            //   transform.position = Vector3.MoveTowards(FakeMyTransform, FakePlayerTransform, speed * Time.deltaTime);
            if (agent.isOnNavMesh)
            {
                agent.SetDestination(FakePlayerTransform);
            }
            
        }

        if (Vector3.Distance(transform.position, Player.position) <= stoppingDistance && Vector3.Distance(transform.position, Player.position) > retreatDistance && charControl.hasHitten == false )
        {
            agent.ResetPath();
            timer = 1f;
            // EnemyAnim.SetBool("shoot", true);
            EnemyAnim.SetBool("run", false);

            transform.position = this.transform.position;
            if (timeBtwShots <= 0)
            {

                Shoot();
                timeBtwShots = startTimeBtwShots;
            }
            else
            {
                EnemyAnim.SetBool("shoot", false);
                EnemyAnim.SetBool("run", false);
                EnemyAnim.SetBool("stop", true);

                //   EnemyAnim.SetBool("run", true);


                timeBtwShots -= Time.deltaTime;
            }

            //  yield return new WaitForSeconds(0.4f);
        }
        if (Vector3.Distance(transform.position, Player.position) < retreatDistance)
        {
            transform.position = Vector3.MoveTowards(FakeMyTransform, FakePlayerTransform, -speed * Time.deltaTime);

        }

    }
    void Shoot()
    {
        if (hasGun && !died && canAttack)
        {
            FireSound.Play();
            StartCoroutine(muzzleFlash.ActivatePistol());
            
            GameObject bullet = Instantiate(Projectile, ProjectileExitPlace.position, Quaternion.identity);
            bullet.transform.eulerAngles = transform.eulerAngles;
            EnemyAnim.SetBool("shoot", true);
        }

    }
    void takePunch()
    {
        if (charControl.hasHitten)
        {

            die();
            charControl.hasHitten = false;




            hasGun = false;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "hitTrigger")
        {

            takePunch();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            die();
            Destroy(collision.gameObject);
           // enemyScript.enabled = false;

        }
    }
    void die()
    {
        Destroy(gameObject, 10);
        DeathSound.Play();
        Blood.SetActive(true);
        EnemyAnim.SetBool("die", true);
        // enemyScript.enabled = false;
        died = true;
        gunScript.enemyhand = false;
        gunRb.isKinematic = false;
        gun.transform.parent = null;
        gun.transform.eulerAngles = new Vector3(90, 0, 0);
        this.enemyRB.isKinematic = false;
        this.enemyCol.enabled = false;
        enemyBoxCol.enabled = false;
        DieCol.SetActive(true);
        agent.ResetPath();
       // this.enemyScript.enabled = false;


    }
    void DuvarinArkasindanAtesEtmemek()
    {

        float range = 6f;
        RaycastHit hit;
        Vector3 high = new Vector3(0, 0.5f, 0);
        Ray ray = new Ray(transform.position + high, transform.forward);
        if (Physics.Raycast(ray, out hit, range , playerLayerMask))
        {           
            canAttack = false;
            
            //Debug.DrawRay(transform.position, transform.forward, Color.black);
        }
        else
        {
            canAttack = true;
            stoppingDistance = 6f;
        }
        

    }
}
