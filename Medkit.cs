using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medkit : MonoBehaviour
{
    int Health = 50;
     CharControl charControl;
    public float RotateSpeed = 20f;
    private void Awake()
    {
        charControl = GameObject.FindGameObjectWithTag("Player").GetComponent<CharControl>();
    }
    void Update()
    {
        transform.Rotate(new Vector3(0, RotateSpeed , 0));
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "TakeHealth")
        {
            if (charControl.currentHealth < 100)
            {
                charControl.TakeHealth(Health);
                Destroy(gameObject);
            }
            
        }
    }
}
