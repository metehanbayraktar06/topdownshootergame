using UnityEngine;
using UnityEngine.AI;



[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(CapsuleCollider))]



public class ZombieBoss : MonoBehaviour
{
    private string CurrentState;
    void ChangeAnimationState(string newState)
    {
        if (CurrentState == newState)
        {
            return;
        }
        EnemyAnim.Play(newState);

        CurrentState = newState;
    }
    const string IdleAnim = "Zombie_Idle";
    const string RunAnim = "Zombie_Run";
    const string AttackAnim = "Zombie_Attack";
    const string DeathAnim = "Zombie_Death";


    public HealthBar healthBar;
    public int MaxHealth = 500;
    public int currentHealth;


    [Header("Attributes")]
    [SerializeField]
    private float speed = 3;
    [SerializeField]
    private float stoppingDistance = 6;
    [SerializeField]
    private float retreatDistance = 0;
    [SerializeField]
    private float timeBtwShots = 2;
    [SerializeField]
    private float startTimeBtwShots;



    public Transform Player;

    private Vector3 FakePlayerTransform;
    private Vector3 FakeMyTransform;
    [Header("Components")]
    public CharControl charControl;
    Animator EnemyAnim;
    NavMeshAgent agent;
    public CapsuleCollider enemyCapsCol;
    

    public CapsuleCollider CapsuleCollider;
    // this must be in kick;
    public CapsuleCollider CapsuleKickCollider;
    ZombieBoss enemyScript;
    Rigidbody enemyRB;
    [Header("Out Objects")]
    //bunlar objenin kendi içinden geliyor:
    public GameObject DieCol;
    public AudioSource DeathSound;
    public AudioSource hitSound;
    public GameObject Blood;
    bool movimentBool;
    bool isAttackFinished;

    [Space]
    public bool hasGun;
    public bool died;
    float timer = 1;
    Vector3 lookat;
    bool eXecuteOnce;
    bool canAttack = true;
    // this is different than timeBetweenShots
    float timeBtw;
    // public GameObject Gun;
    int damage;
    bool onceExecute = false;
    void Start()
    {
        currentHealth = MaxHealth;
        healthBar.SetMaxHealth(MaxHealth);


        died = false;

        EnemyAnim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
       

        enemyScript = GetComponent<ZombieBoss>();
        enemyRB = GetComponent<Rigidbody>();
        ChangeAnimationState(IdleAnim);


        hasGun = true;
        // Player = GameObject.FindGameObjectWithTag("Player").transform;

    }


    void Update()
    {

        if (currentHealth <= 0 && !onceExecute)
        {
            onceExecute = true;
            die();
        }
        timeBtw -= Time.deltaTime;
        if (!canAttack && timeBtw <= 0)
        {
            stoppingDistance--;
            timeBtw = 0.1f;
        }
        lookat = new Vector3(Player.position.x, transform.position.y, Player.position.z);

        if (died)
        {

            transform.position = this.transform.position;
        }
        if (!died)
        {
            transform.LookAt(lookat);

        }
        // enemy playera sabit bakıyor ve onun son pozisyonlarını alıyor.
        FakePlayerTransform = new Vector3(Player.position.x, 0, Player.position.z);
        FakeMyTransform = new Vector3(transform.position.x, 0, transform.position.z);

        timer -= Time.deltaTime;
        // StartCoroutine(EnemyMovementAndAnims());



        if (died == false)
        {
            EnemyMovementAndAnims();
        }
    }


    void EnemyMovementAndAnims()
    {


        if (Vector3.Distance(transform.position, Player.position) > stoppingDistance && timer < 0)
        {

            ChangeAnimationState(RunAnim);
            CapsuleKickCollider.enabled = false;

            //   transform.position = Vector3.MoveTowards(FakeMyTransform, FakePlayerTransform, speed * Time.deltaTime);
            if (agent.isOnNavMesh)
            {
                agent.SetDestination(FakePlayerTransform);
            }

        }



        if (Vector3.Distance(transform.position, Player.position) <= stoppingDistance && Vector3.Distance(transform.position, Player.position) > retreatDistance && charControl.hasHitten == false)
        {
            agent.ResetPath();
            timer = 1.5f;


            ChangeAnimationState(AttackAnim);
            CapsuleKickCollider.enabled = true;




            transform.position = this.transform.position;
            if (timeBtwShots <= 0)
            {



                timeBtwShots = startTimeBtwShots;
            }
            else
            {


                //   EnemyAnim.SetBool("run", true);


                timeBtwShots -= Time.deltaTime;
            }

            //  yield return new WaitForSeconds(0.4f);
        }
        if (Vector3.Distance(transform.position, Player.position) < retreatDistance)
        {
            transform.position = Vector3.MoveTowards(FakeMyTransform, FakePlayerTransform, -speed * Time.deltaTime);

        }

    }

    void takePunch()
    {
        if (charControl.hasHitten)
        {
            damage = 20;
            hitZombie(damage);
            charControl.hasHitten = false;
            hasGun = false;
        }
    }
    public void hitZombie(int damage)
    {
        currentHealth -= damage;
        hitSound.Play();
        healthBar.SetHealth(currentHealth);
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "hitTrigger")
        {

            takePunch();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            damage = 40;
            hitZombie(damage);
            Destroy(collision.gameObject);
            // enemyScript.enabled = false;

        }
    }
    void die()
    {
        Destroy(gameObject, 10);
        DeathSound.Play();
        Blood.SetActive(true);
        // enemyScript.enabled = false;
        ChangeAnimationState(DeathAnim);
        died = true;

        this.enemyRB.isKinematic = false;
        this.enemyCapsCol.enabled = false;
        CapsuleCollider.enabled = false;
        DieCol.SetActive(true);
        agent.ResetPath();
        CapsuleKickCollider.enabled = false;
       enemyScript.enabled = false;


    }

}